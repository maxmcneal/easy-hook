/**
 * Example: Demonstrating a Usermode Function Hook using the EasyHook library
 * ==================================================================================
 *
 * Overview:
 * This example showcases the process of hooking a simple function in C++ using the easyhook library,
 * designed to modify the runtime behavior of programs for various purposes such as debugging, extending
 * functionality, or monitoring specific operations. The core idea demonstrated here is intercepting
 * calls to an original function (calculate_square), and redirecting them to a custom function
 * (calculate_cube_instead) that alters the program's behavior.
 *
 * The example operates in several steps:
 * 1. Definition of an original function that calculates the square of an integer.
 * 2. Setup of a hooked function that, instead of squaring, calculates the cube of the input integer.
 * 3. Application of the hook to redirect calls from the original square function to the new cube function.
 * 4. Demonstration of enabling and disabling the hook to compare the original and modified behaviors.
 *
 * Key Concepts:
 * - **Function Hooking:** A technique used to intercept calls to functions during runtime, allowing for behavior
 *   modification, monitoring, or extension without changing the original function's code. It's widely used in
 *   programming for various purposes, including debugging, customizing software behavior, or injecting new
 *   functionality into existing binary executables.
 *
 * - **Trampolining:** To ensure that the original functionality can still be accessed after a function is hooked,
 *   a "trampoline" is created. This is essentially a piece of dynamically generated code that jumps to the
 *   original function's body. In our example, this allows the hooked function to optionally call the original
 *   function's logic (i.e., calculating the square) before or after applying its custom logic (i.e., calculating
 *   the cube).
 *
 * Usage and Application:
 * By following this example, developers can learn how to implement basic function hooking in their applications
 * or tools, enabling them to extend or alter the behavior of existing code dynamically, without direct modifications
 * or recompilation. This technique is invaluable for creating extendable applications, developing plugins,
 * or implementing custom debugging and monitoring tools.
 *
 * Note: This example assumes the presence of a the easy hook library defined in "easy_hook.hpp", which
 * provides the necessary infrastructure for creating, enabling, and disabling hooks. 
 */

#include "easy_hook.hpp" // Ensure this path matches the location of your hooking system header
#include <iostream>
#include <memory>

// Original function signature
using square_function_t = int(*)(int number);

// Global hook information instance for the square function
static hook_information square_hook;

// Original function definition
__declspec(noinline) int calculate_square(int number) {
    int result = number * number;
    std::cout << "Original square: " << result << '\n';
    return result;
}

// Hooked function definition
__declspec(noinline) int calculate_cube_instead(int number) {
    std::cout << "Hooked to calculate cube!\n";
    
    // Directly calling the trampoline to get the original behavior if needed
    auto original_square = reinterpret_cast<square_function_t>(square_hook.trampoline.get());
    int original_result = original_square(number);

    // Modifying behavior: instead of returning the square, return the cube
    int cube_result = number * number * number;
    std::cout << "Modified to cube: " << cube_result << '\n';
    return cube_result;
}

int main() {
    // Demonstrate the original function's behavior
    int number = 3;
    int result = calculate_square(number);
    std::cout << "Result before hooking: " << result << '\n';

    // Setup and enable the hook
    square_hook = create_hook(reinterpret_cast<void*>(&calculate_square), reinterpret_cast<void*>(&calculate_cube_instead));
    bool status = enable_hook(square_hook);
    std::cout << "Hooking status: " << status << '\n';

    // Test the function with the hook enabled
    result = calculate_square(number);
    std::cout << "Result after hooking: " << result << '\n';

    // Disable the hook and test again
    status = disable_hook(square_hook);
    std::cout << "Disabling hook status: " << status << '\n';
    result = calculate_square(number);
    std::cout << "Result after disabling hook: " << result << '\n';

    return 0;
}
