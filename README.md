<div align="center">
    <h1>
        <img src="images/header.png" href="https://air-quality-api-maxmcneal-60679c1ff45b2f3194f331335e7e62e59596a.gitlab.io" alt="Easy Hook">
    </h1>
    <h4>
        A minimal, single-header library for hooks, crafted in pure C and designed for cross-platform compatibility on x86-64 systems.
    </h4>
</div>

<div align="center">
  <img src="https://img.shields.io/github/v/release/nhn/tui.editor.svg?include_prereleases">
  <img src="https://img.shields.io/github/license/nhn/tui.editor.svg">
  <img src="https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5-ff1414.svg">
  <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
</div>

<div align="center">
  <a href="#-goals-and-requirements">Goals and Requirements</a> •
  <a href="#-key-learnings">Key Learnings</a> •
  <a href="#-Usage">Usage</a> •
  <a href="#-contributing">Contributing</a> •
  <a href="#-license">License</a>
</div>

<br/>

![screenshot](images/banner.png)

## 🎯 Goals and Requirements

- Enable hooking functionality in x86-64 applications across different platforms.
- Ensure minimal overhead to keep the library lightweight.
- Provide a single-header file for easy integration into projects.
- Support pure C to enhance compatibility and ease of use.
- Implement cross-platform capabilities for Windows, Linux, and macOS.
- Facilitate both code injection and function interception.
- Offer documentation and examples for common use cases.
- Design for extensibility to support future architectures or platforms.
- Prioritize performance and reliability in hook implementation.
- Include a testing suite to verify functionality across environments.
- Ensure security practices to prevent malicious use.
- Allow customization and configuration for advanced use cases.

## 📙 Key learnings

- Cross-platform development challenges
- x86-64 architecture specifics
- Hooking techniques and their applications
- Memory management in C
- Debugging and optimization strategies
- API design for ease of use
- Security considerations for hooking
- Performance impacts of hooks
- Testing methodologies for system-level code

## ⚙️ Usage

Include the header file in your project. Initialise hook structure by calling CreateHook. Perform the actual hook by calling EnableHook and optionally revert the hook with a call to DisableHook.

### Prerequisites

- A C++ compiler supporting C++17 or later.
- Basic understanding of C++ function pointers and casting.

### Step 1: Including the Header

First, ensure the `easy_hook.hpp` file is in your project's include path. Then, include it in your source file:

```cpp
#include "easy_hook.hpp"
```

### Step 2: Initialize the Hook

Define the original function and the function you want to call instead. Then, initialize a hook structure by calling `create_hook`, passing the addresses of these functions.

```cpp
void original_function(int a, int b);
void new_function(int a, int b);

hook_information info = create_hook(reinterpret_cast<void*>(&original_function), reinterpret_cast<void*>(&new_function));
```

### Step 3: Enable the Hook

Activate the hook with enable_hook. This redirects calls from the original function to the new function.

```cpp
bool status = enable_hook(info);
if (!status) {
    std::cerr << "Failed to enable the hook." << std::endl;
    return; // Handle the error appropriately
}
```

### Step 4: Invoke the Original Function via the Trampoline

Even after the hook is enabled, you can still invoke the original function using the trampoline provided in the hook_information structure.

```cpp
auto original = reinterpret_cast<void(*)(int, int)>(info.trampoline.get());
original(10, 20); // This calls the original function
```

### Step 5: Disable the Hook

To disable the hook and restore the original function's behavior, call disable_hook.

```cpp
disable_hook(info);
```


## 🐛 Contributing

- TBA

## 💭 Credits

- TBA

## 📜 License

This project is licensed under the terms of the MIT license and protected by Udacity Honor Code and Community Code of Conduct. See <a href="LICENSE.md">license</a> and <a href="LICENSE.DISCLAIMER.md">disclaimer</a>.

