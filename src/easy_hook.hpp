#ifndef EASY_HOOK_HPP
#define EASY_HOOK_HPP

#include <cstdint>
#include <array>
#include <cstring> 
#include <memory> 
#include <vector> 

// Macros for extracting row and column from a byte, used to process REX prefix and opcode parts.
#define HOOK_ROW (*current_byte >> 4)
#define HOOK_COLUMN (*current_byte & 0xF)

// Prefixes that modify the behavior of the instructions in x86_64 architecture.
static const std::array<uint8_t, 11> instruction_prefixes = {
    0xF0, 0xF2, 0xF3, 0x2E, 0x36, 0x3E, 0x26, 0x64, 0x65, 0x66, 0x67
};

// Opcode bytes that specify an operation requiring a ModR/M byte.
static const std::array<uint8_t, 18> opcodes_modrm = {
    0x62, 0x63, 0x69, 0x6B, 0xC0, 0xC1, 0xC4, 0xC5, 0xC6, 0xC7, 0xD0, 0xD1, 0xD2, 0xD3, 0xF6, 0xF7, 0xFE, 0xFF
};

// Opcode bytes that are followed by an immediate value of 8 bits.
static const std::array<uint8_t, 13> opcodes_imm8 = {
    0x6A, 0x6B, 0x80, 0x82, 0x83, 0xA8, 0xC0, 0xC1, 0xC6, 0xCD, 0xD4, 0xD5, 0xEB
};

// Opcode bytes that are followed by an immediate value of 32 bits.
static const std::array<uint8_t, 7> opcodes_imm32 = {
    0x68, 0x69, 0x81, 0xA9, 0xC7, 0xE8, 0xE9
};

// Opcode bytes that specify an operation requiring a second ModR/M byte.
static const std::array<uint8_t, 9> opcodes_modrm_ext = {
    0x0D, 0xA3, 0xA4, 0xA5, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF
};

/**
 * Searches for a specific byte within a buffer.
 * 
 * @param buffer The buffer to search.
 * @param max_length The maximum length of the buffer.
 * @param value The byte value to search for.
 * @return True if the byte is found, false otherwise.
 */
static bool find_byte(const uint8_t* buffer, uint64_t max_length, uint8_t value) {
    for (uint64_t i = 0; i < max_length; ++i) {
        if (buffer[i] == value) {
            return true;
        }
    }
    return false;
}

/**
 * Parses the ModR/M byte to adjust the buffer pointer accordingly, handling SIB and displacement bytes.
 * 
 * @param buffer Pointer to the current position in the buffer.
 * @param address_prefix Indicates if the instruction has a legacy address size overwrite prefix.
 */
static void parse_modrm(uint8_t** buffer, bool address_prefix) {
    const uint8_t mod_rm = *++*buffer;

    if (!address_prefix || (address_prefix && **buffer >= 0x40)) {
        bool has_sib = false;
        if (**buffer < 0xC0 && (**buffer & 0x07) == 0x04 && !address_prefix) {
            has_sib = true;
            ++(*buffer);
        }

        if (mod_rm >= 0x40 && mod_rm <= 0x7F) {
            ++(*buffer);
        } else if ((mod_rm <= 0x3F && (mod_rm & 0x07) == 0x05) || (mod_rm >= 0x80 && mod_rm <= 0xBF)) {
            *buffer += address_prefix ? 2 : 4;
        } else if (has_sib && (**buffer & 0x07) == 0x05) {
            *buffer += (mod_rm & 0x40) ? 1 : 4;
        }
    } else if (address_prefix && mod_rm == 0x26) {
        *buffer += 2;
    }
}

/**
 * Calculates the size of a basic instruction for the x86_64 (AMD64) platform.
 * 
 * @param address The address of the instruction.
 * @return The size of the instruction in bytes.
 */
static int get_instruction_size(const void* address) {
    uint64_t offset = 0;
    bool operand_prefix = false, address_prefix = false, rex_w = false;
    uint8_t* current_byte = (uint8_t*)address;

    // Iterate through prefix bytes
    for (int i = 0; i < 14 && (find_byte(instruction_prefixes.data(), instruction_prefixes.size(), *current_byte) || HOOK_ROW == 4); ++i, ++current_byte) {
        if (*current_byte == 0x66) {
            operand_prefix = true;
        } else if (*current_byte == 0x67) {
            address_prefix = true;
        } else if (HOOK_ROW == 4 && HOOK_COLUMN >= 8) {
            rex_w = true;
        }
    }

    // Handle opcode bytes and their specific cases
    if (*current_byte == 0x0F) {
        ++current_byte;
        if (*current_byte == 0x38 || *current_byte == 0x3A) {
            if (*current_byte++ == 0x3A) {
                offset++;
            }
            parse_modrm(&current_byte, address_prefix);
        } else {
            if (HOOK_ROW == 8) {
                offset += 4;
            } else if ((HOOK_ROW == 7 && HOOK_COLUMN < 4) || *current_byte == 0xA4 || *current_byte == 0xC2 || (*current_byte > 0xC3 && *current_byte <= 0xC6) || *current_byte == 0xBA || *current_byte == 0xAC) {
                offset++;
            }

            if (find_byte(opcodes_modrm_ext.data(), opcodes_modrm_ext.size(), *current_byte) || (HOOK_ROW != 3 && HOOK_ROW > 0 && HOOK_ROW < 7) || *current_byte >= 0xD0 || (HOOK_ROW == 7 && HOOK_COLUMN != 7) || HOOK_ROW == 9 || HOOK_ROW == 0xB || (HOOK_ROW == 0xC && HOOK_COLUMN < 8) || (HOOK_ROW == 0 && HOOK_COLUMN < 4)) {
                parse_modrm(&current_byte, address_prefix);
            }
        }
    } else {
        if ((HOOK_ROW == 0xE && HOOK_COLUMN < 8) || (HOOK_ROW == 0xB && HOOK_COLUMN < 8) || HOOK_ROW == 7 || (HOOK_ROW < 4 && (HOOK_COLUMN == 4 || HOOK_COLUMN == 0xC)) || (*current_byte == 0xF6 && !(*(current_byte + 1) & 0x30)) || find_byte(opcodes_imm8.data(), opcodes_imm8.size(), *current_byte)) {
            offset++;
        } else if (*current_byte == 0xC2 || *current_byte == 0xCA) {
            offset += 2;
        } else if (*current_byte == 0xC8) {
            offset += 3;
        } else if ((HOOK_ROW < 4 && (HOOK_COLUMN == 5 || HOOK_COLUMN == 0xD)) || (HOOK_ROW == 0xB && HOOK_COLUMN >= 8) || (*current_byte == 0xF7 && !(*(current_byte + 1) & 0x30)) || find_byte(opcodes_imm32.data(), opcodes_imm32.size(), *current_byte)) {
            offset += rex_w ? 8 : (operand_prefix ? 2 : 4);
        } else if (HOOK_ROW == 0xA && HOOK_COLUMN < 4) {
            offset += rex_w ? 8 : (address_prefix ? 2 : 4);
        } else if (*current_byte == 0xEA || *current_byte == 0x9A) {
            offset += operand_prefix ? 4 : 6;
        }

        if (find_byte(opcodes_modrm.data(), opcodes_modrm.size(), *current_byte) || (HOOK_ROW < 4 && (HOOK_COLUMN < 4 || (HOOK_COLUMN >= 8 && HOOK_COLUMN < 0xC))) || HOOK_ROW == 8 || (HOOK_ROW == 0xD && HOOK_COLUMN >= 8)) {
            parse_modrm(&current_byte, address_prefix);
        }
    }

    return static_cast<int>((++current_byte + offset) - static_cast<uint8_t*>(const_cast<void*>(address)));
}

/**
 * Copies memory from source to destination.
 * 
 * @param destination The target memory address to copy data into.
 * @param source The source memory address to copy data from.
 * @param size The number of bytes to copy.
 */
static void memory_copy(void* destination, const void* source, std::size_t size) {
    std::memcpy(destination, source, size);
}

/**
 * Hook information structure to manage hook state, original and target function pointers,
 * and the trampoline for executing original instructions.
 */
struct hook_information {
    bool enabled = false;
    std::size_t bytes_to_copy = 0;
    std::array<uint8_t, 32> original_buffer{};
    void* original_function = nullptr;
    void* target_function = nullptr;
    std::unique_ptr<uint8_t[]> trampoline;

    // Default constructor
    hook_information() = default;

    // Constructor to initialize hook information
    hook_information(void* orig_func, void* targ_func)
        : original_function(orig_func), target_function(targ_func) {}
};

// Definition for jump code used in creating hooks.
static const std::array<uint8_t, 14> jump_code = {
    0xFF, 0x25, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};

/**
 * Platform-specific memory allocation that grants read, write, and execute permissions.
 * 
 * @param size The number of bytes to allocate.
 * @return A pointer to the allocated memory region.
 */
static void* platform_allocate(std::size_t size) {
    // Platform-specific implementation (Windows, Linux, EFI)
    #ifdef _WIN32
    return VirtualAlloc(nullptr, size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
    #elif defined(__linux__) || defined(__unix__)
    return mmap(nullptr, size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    #else
    // Fallback or custom allocation logic for other platforms
    static_assert(false, "Unsupported platform for memory allocation.");
    #endif
}

/**
 * Platform-specific memory deallocation.
 * 
 * @param address Pointer to the memory region to free.
 * @param size The size of the memory region. This parameter might be unused depending on the platform.
 */
static void platform_free(void* address, std::size_t size) {
    // Platform-specific implementation (Windows, Linux, EFI)
    #ifdef _WIN32
    VirtualFree(address, 0, MEM_RELEASE);
    #elif defined(__linux__) || defined(__unix__)
    munmap(address, size);
    #else
    // Fallback or custom deallocation logic for other platforms
    static_assert(false, "Unsupported platform for memory deallocation.");
    #endif
}

/**
 * Changes memory protection flags to grant read, write, and execute permissions.
 * 
 * @param address The memory address whose protection flags to change.
 * @param size The size of the memory region. This might be rounded up to the nearest page size.
 * @return The original protection flags. This return value is platform-specific.
 */
static auto platform_protect(void* address, std::size_t size) -> unsigned long {
    // Platform-specific implementation (Windows, Linux, EFI)
    #ifdef _WIN32
    DWORD old_protection;
    VirtualProtect(address, size, PAGE_EXECUTE_READWRITE, &old_protection);
    return old_protection;
    #elif defined(__linux__) || defined(__unix__)
    long page_size = sysconf(_SC_PAGESIZE);
    void* aligned_address = reinterpret_cast<void*>(reinterpret_cast<std::size_t>(address) & ~(page_size - 1));
    mprotect(aligned_address, size + (address - aligned_address), PROT_READ | PROT_WRITE | PROT_EXEC);
    return PROT_READ | PROT_WRITE | PROT_EXEC; // Simplification, as original protection is not easily retrieved.
    #else
    // Fallback or custom protection logic for other platforms
    static_assert(false, "Unsupported platform for memory protection.");
    #endif
}

/**
 * Prepares the hook by copying the original function's bytes and setting up a trampoline.
 * 
 * @param original_function Function to hook.
 * @param target_function Function that will be called instead.
 * @return Hook information structure with details about the hook.
 */
hook_information create_hook(void* original_function, void* target_function) {
    hook_information info(original_function, target_function);

    // Calculate the number of bytes to copy based on the size of the jump code and the instruction sizes
    std::size_t size = 0;
    while (size < jump_code.size()) {
        size += get_instruction_size(static_cast<uint8_t*>(original_function) + size);
    }

    info.bytes_to_copy = size;
    // Copy the original function bytes to the buffer in the hook information
    memory_copy(info.original_buffer.data(), original_function, size);

    return info;
}

/**
 * Enables the hook by creating a trampoline and overwriting the original function's beginning with a jump to the target function.
 * 
 * @param info Reference to the hook information structure.
 * @return True if the hook was successfully enabled, false otherwise.
 */
bool enable_hook(hook_information& info) {
    if (info.enabled) {
        return true; // The hook is already enabled
    }

    // Allocate memory for the trampoline
    const std::size_t buffer_size = jump_code.size() + info.bytes_to_copy;
    auto trampoline_memory = std::make_unique<uint8_t[]>(buffer_size);
    if (!trampoline_memory) {
        return false; // Memory allocation failed
    }

    // Setup the trampoline: copy original bytes and append a jump back to the original function
    memory_copy(trampoline_memory.get(), info.original_buffer.data(), info.bytes_to_copy);
    auto jump_back_address = reinterpret_cast<uintptr_t>(info.original_function) + info.bytes_to_copy;
    auto jump_back = generate_jump(jump_code, jump_back_address);
    memory_copy(trampoline_memory.get() + info.bytes_to_copy, jump_back.data(), jump_code.size());

    // Write the jump to the target function at the start of the original function
    auto jump_to_target = generate_jump(jump_code, reinterpret_cast<uintptr_t>(info.target_function));
    auto original_protection = platform_protect(info.original_function, buffer_size);
    memory_copy(info.original_function, jump_to_target.data(), jump_code.size());
    platform_protect(info.original_function, buffer_size, original_protection);

    // Update the hook information
    info.trampoline = std::move(trampoline_memory);
    info.enabled = true;

    return true;
}

/**
 * Disables the hook by restoring the original function's bytes and freeing the trampoline memory.
 * 
 * @param info Reference to the hook information structure.
 * @return True if the hook was successfully disabled, false otherwise.
 */
bool disable_hook(hook_information& info) {
    if (!info.enabled) {
        return true; // The hook is already disabled
    }

    // Restore the original function bytes
    auto original_protection = platform_protect(info.original_function, info.bytes_to_copy);
    memory_copy(info.original_function, info.original_buffer.data(), info.bytes_to_copy);
    platform_protect(info.original_function, info.bytes_to_copy, original_protection);

    // Free the trampoline memory
    info.trampoline.reset(); // unique_ptr automatically deallocates memory

    info.enabled = false;
    return true;
}

/**
 * Generates a jump instruction to a given address using the specified jump code template.
 * 
 * @param jump_template The template for the jump instruction.
 * @param target_address The target address to jump to.
 * @return A vector containing the bytes of the jump instruction.
 */
std::vector<uint8_t> generate_jump(const std::array<uint8_t, 14>& jump_template, uintptr_t target_address) {
    std::vector<uint8_t> jump(jump_template.begin(), jump_template.end());
    *reinterpret_cast<uintptr_t*>(&jump[6]) = target_address; // Insert the target address into the jump instruction
    return jump;
}

#endif // EASY_HOOK_HPP